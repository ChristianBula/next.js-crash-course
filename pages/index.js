import { MongoClient } from 'mongodb';
import MeetupList from "../components/meetups/MeetupList";
import Head from 'next/head';
import { Fragment } from 'react';

function HomePage(props) {
    return(
    <Fragment>
     <Head>
         <title>React Meetups</title>
         <meta 
          name='description'
          content='Browse different meetups'
          />
     </Head>
     <MeetupList meetups={props.meetups} />
    </Fragment>
    )
}

export async function getStaticProps(){
    const client = await MongoClient.connect('mongodb+srv://john_david:password123321@cluster0.i8gze.mongodb.net/myFirstDatabase?retryWrites=true&w=majority');
    const db = client.db();
    const meetupsCollection = db.collection('meetups');

    const meetups = await meetupsCollection.find().toArray()

    client.close();

    return {
        props: {
            meetups: meetups.map(meetup => ({
                title: meetup.title,
                address: meetup.address,
                image: meetup.image,
                id: meetup._id.toString(),
            }))
        },
        revalidate: 10
    };
}

export default HomePage;
