import { MongoClient } from 'mongodb'

async function handler(req,res){
    if(req.method === 'POST'){
        const data = req.body;

        const {title, image, address, desc} = data;

        const client = await MongoClient.connect('mongodb+srv://john_david:password123321@cluster0.i8gze.mongodb.net/myFirstDatabase?retryWrites=true&w=majority');
        const db = client.db();

        const meetupsCollection = db.collection('meetups');

        const result = await meetupsCollection.insertOne(data);

        console.log(result)

        client.close();

        res.status(201).json({message: 'Meetup Inserted!'});
    }
}

export default handler;